/**
 * SE2811
 * Winter 2017
 * Lab 3 The Flowers and the Bees - BumbleBee Class
 * Name: Noe Gonzalez & Trevor Suarez
 * Created: 1/9/2018
 */

package se2811;

import javafx.scene.image.ImageView;

import java.util.ArrayList;
import java.util.Random;

/**
 * @author Trevor Suarez
 * BumbleBee provides functionality for position and energy values for the BumbleBee, the random bee
 */
public class BumbleBee extends Bee {

    private int energy;
    private ArrayList<Flower> flowers;
    private ImageView image;
    private double x;
    private double y;
    private int speed;
    private Random random;

    /**
     * BumbleBee constructor initializes all bumble bee specific instance variables
     *
     * @param image
     */
    public BumbleBee(ImageView image) {
        random = new Random();
        this.energy = 500;
        this.image = image;
        this.x = image.getLayoutX();
        this.y = image.getLayoutY();
        this.speed = 50;


    }

    public void setFlowers(ArrayList<Flower> flowers) {
        this.flowers = flowers;
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int newEnergy) {
        energy = newEnergy;
    }

    /**
     * calculateRandCoordinate calculates the random flower it will be heading to next
     *
     * @return flower index     the index of the flower selected
     */
    @Override
    public Flower calculateRandCoordinate() {
        int index = random.nextInt(flowers.size());
        return flowers.get(index);
    }

    /**
     * modifyEnergy subtracts one energy value when one tick is completed
     */
    public void modifyEnergy() {
        energy = energy - 1;
    }

    /**
     * addEnergy adds energy when hovered over flower
     *
     * @param addEnergy amount of energy being added to bee
     */
    public void addEnergy(int addEnergy) {
        energy = energy + addEnergy;
    }

    public double getSpeed() {
        return speed;
    }
}