package se2811;

import java.util.ArrayList;

public class ObliviousBee extends BeeDecorator {
    private Bee bee;
    private ArrayList<Flower> flowers;

    public ObliviousBee(Bee bee) {
        super(bee);
        this.bee = bee;
    }

    public void setFlowers(ArrayList<Flower> flowers) {
        this.flowers = flowers;
    }

    @Override
    public int getEnergy() {
        return bee.getEnergy();
    }

    @Override
    public void setEnergy(int newEnergy) {
        bee.setEnergy(newEnergy);
    }

    /**
     * modifyEnergy subtracts one energy value when one tick is completed
     */
    @Override
    public void modifyEnergy() {
        bee.modifyEnergy();
    }

    /**
     * addEnergy adds energy when hovered over flower
     *
     * @param addEnergy amount of energy being added to bee
     */
    @Override
    public void addEnergy(int addEnergy) {
        bee.setEnergy(addEnergy);
    }

    @Override
    public double getSpeed() {
        return bee.getSpeed();
    }

    @Override
    public int getSpeedMultiplier() {
        return bee.getSpeedMultiplier();
    }
}
