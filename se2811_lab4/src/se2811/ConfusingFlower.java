package se2811;


import javafx.scene.image.ImageView;

public class ConfusingFlower extends Flower {
    private ImageView image;
    private int energy;
    private int count;

    public ConfusingFlower(ImageView image) {
        this.image = image;
        this.energy = 30;
        this.count = 0;
    }


    /**
     * determineEnergy returns energy of venustrap
     *
     * @return energy   energy of venustrap
     */
    @Override
    public int determineEnergy() {
        if (count < 30) {
            count++;
        } else {
            setEnergy(0);
        }
        return energy;
    }

    public double getX() {
        return image.getLayoutX();
    }

    public double getY() {
        return image.getLayoutY();
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }
}