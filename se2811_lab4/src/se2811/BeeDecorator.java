package se2811;

public abstract class BeeDecorator extends Bee {
    private double speed;
    protected Bee wrappedBee;

    public BeeDecorator(Bee bee) {
        this.wrappedBee = bee;
        this.speed = 50;
    }

    public BeeDecorator() {

    }

    public double getSpeed() {
        return speed;
    }
}
