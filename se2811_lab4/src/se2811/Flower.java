/**
 * SE2811
 * Winter 2017
 * Lab 3 The Flowers and the Bees - Flower Class
 * Name: Noe Gonzalez & Trevor Suarez
 * Created: 1/9/2018
 */

package se2811;

/**
 * @author Noe Gonzalez
 * @version 1.0
 * @created 14-Dec-2017 3:52:48 PM
 * Flower provides basic implementation of a flower, with general behavior
 */
public abstract class Flower extends GardenObject {

    private int energy;
    private boolean visited;
    private double x;
    private double y;

    /**
     * Flower constructor initializes flower instance variables
     */
    public Flower() {
        this.visited = false;
        this.x = 10;
        this.y = 10;
    }

    /**
     * Makes the flower visited therefore out of nectar.
     */
    public void visit() {
        this.visited = true;
        this.energy = 0;
    }

    /**
     * determineEnergy determines energy to assign to bee
     * @return 0    generic method implementation
     */
    public int determineEnergy(){
        return 0;
    }


}