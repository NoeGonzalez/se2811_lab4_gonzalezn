package se2811;

public class SpeedyBee extends BeeDecorator {
    private Bee bee;
    private int speed;
    private int speedMultiplier;

    public SpeedyBee(Bee bee) {
        super(bee);
        this.bee = bee;
        this.speed = 25;
        this.speedMultiplier = 2;
    }

    @Override
    public int getEnergy() {
        return bee.getEnergy();
    }

    @Override
    public void setEnergy(int newEnergy) {
        bee.setEnergy(newEnergy);
    }

    /**
     * modifyEnergy subtracts one energy value when one tick is completed
     */
    @Override
    public void modifyEnergy() {
        bee.modifyEnergy();
    }

    /**
     * addEnergy adds energy when hovered over flower
     *
     * @param addEnergy amount of energy being added to bee
     */
    @Override
    public void addEnergy(int addEnergy) {
        bee.setEnergy(addEnergy);
    }

    @Override
    public double getSpeed() {
        return bee.getSpeed() / 1.5;
    }

    @Override
    public int getSpeedMultiplier() {
        return speedMultiplier;
    }
}
