/**
 * SE2811
 * Winter 2017
 * Lab 3 The Flowers and the Bees - Bee Class
 * Name: Noe Gonzalez & Trevor Suarez
 * Created: 1/9/2018
 */

package se2811;


import java.util.ArrayList;
import java.util.Random;

/**
 * @author Trevor Suarez
 * Bee class is the parent class of the bumble bee and honey bee, declaring general variables
 */
public abstract class Bee extends GardenObject {

    private int energy;
    private double x;
    private double y;
    private double speed;
    private int speedMultiplier;
    private ArrayList<Flower> flowers;
    private Random random;

    public Bee() {
        this.speedMultiplier = 1;
        this.random = new Random();
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int newEnergy) {
        energy = newEnergy;
    }

    /**
     * modifyEnergy subtracts one energy value when one tick is completed
     */
    public void modifyEnergy() {
        energy = energy - 1;
    }

    /**
     * addEnergy adds energy when hovered over flower
     *
     * @param addEnergy amount of energy being added to bee
     */
    public void addEnergy(int addEnergy) {
        energy = energy + addEnergy;
    }

    public double getSpeed() {
        return speed;
    }

    public int getSpeedMultiplier() {
        return speedMultiplier;
    }

    public void setFlowers(ArrayList<Flower> flowers) {
        this.flowers = flowers;
    }

    /**
     * calculateRandCoordinate calculates the random flower it will be heading to next
     *
     * @return flower index     the index of the flower selected
     */
    public Flower calculateRandCoordinate() {
        int index = random.nextInt(flowers.size());
        return flowers.get(index);
    }

}