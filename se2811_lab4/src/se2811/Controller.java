/**
 * SE2811
 * Winter 2017
 * Lab 3 The Flowers and the Bees - Controller Class
 * Name: Noe Gonzalez & Trevor Suarez
 * Created: 1/9/2018
 */

package se2811;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * @author Trevor Suarez & Noe Gonzalez
 * Controller class connects java functionality with the javafx user interface
 * Controller class also handles
 */
public class Controller implements Initializable {
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private ImageView sunImage1;
    @FXML
    private ImageView sunImage2;
    @FXML
    private ImageView sunImage3;
    @FXML
    private ImageView venusImage1;
    @FXML
    private ImageView venusImage2;
    @FXML
    private ImageView venusImage3;
    @FXML
    private ImageView bumbleImage;
    @FXML
    private ImageView honeyImage;
    @FXML
    private ImageView confusingFlower1;
    @FXML
    private ImageView bumbleLegend;
    @FXML
    private ImageView honeyLegend;
    @FXML
    private ImageView sunflowerLegend;
    @FXML
    private ImageView venusLegend;
    @FXML
    private ImageView confuseLegend;
    @FXML
    private ImageView confusedBeeLegend;
    @FXML
    private ImageView speedyBeeLegend;
    @FXML
    private ImageView shieldedBeeLegend;
    @FXML
    private ImageView confusingFlower2;
    @FXML
    private Label honeyLife;
    @FXML
    private Label bumbleLife;
    @FXML
    private Label welcomeLabel;

    private Bee bumbleBee;
    private Bee honeyBee;

    private Sunflower sunflower1;
    private Sunflower sunflower2;
    private Sunflower sunflower3;
    private VenusTrap venus1;
    private VenusTrap venus2;
    private VenusTrap venus3;
    private ConfusingFlower confuse1;
    private ConfusingFlower confuse2;
    private boolean firstTick;
    private boolean firstObliviousTick;
    private Flower flower;
    private double xDest;
    private double yDest;
    private ArrayList<Flower> flowers;
    private ArrayList<Flower> venusTraps;
    private Image speedy;
    private Image confusedBee;
    private Image shieldedBee;
    private boolean obliviousHoney;
    private boolean obliviousBumble;
    private int counter;
    private double xDestination;
    private double yDestination;
    private Flower tempFlower;

    /**
     * initialize sets all defaults to javafx components
     *
     * @param location
     * @param resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        xDestination = 0;
        yDestination = 0;
        counter = 0;
        firstTick = false;
        firstObliviousTick = false;
        obliviousBumble = false;
        obliviousHoney = false;
        xDest = 0;
        yDest = 0;
        flower = new Sunflower(sunImage2);
        tempFlower = new Sunflower(sunImage2);
        Image sunFlower = new Image("sunflower.png");
        Image venus = new Image("flytrap.png");
        Image bumble = new Image("bumblebee.png");
        Image honey = new Image("honeybee.png");
        Image confuseFlower = new Image("ConfusionFlower.jpg");
        speedy = new Image("fastBee.png");
        confusedBee = new Image("cluelessBee.png");
        shieldedBee = new Image("shieldedBee.png");
        flowers = new ArrayList<>();
        venusTraps = new ArrayList<>();
        sunImage1.setImage(sunFlower);
        sunImage2.setImage(sunFlower);
        sunImage3.setImage(sunFlower);
        venusImage1.setImage(venus);
        venusImage2.setImage(venus);
        venusImage3.setImage(venus);
        bumbleImage.setImage(bumble);
        honeyImage.setImage(new Image("honeybee.png"));
        confusingFlower1.setImage(confuseFlower);
        confusingFlower2.setImage(confuseFlower);
        bumbleLegend.setImage(bumble);
        honeyLegend.setImage(honey);
        confusedBeeLegend.setImage(confusedBee);
        confuseLegend.setImage(new Image("ConfusionFlower.jpg"));
        shieldedBeeLegend.setImage(shieldedBee);
        venusLegend.setImage(venus);
        speedyBeeLegend.setImage(speedy);
        sunflowerLegend.setImage(sunFlower);
        bumbleBee = new BumbleBee(bumbleImage);
        honeyBee = new HoneyBee(honeyImage);
        sunflower1 = new Sunflower(sunImage1);
        sunflower2 = new Sunflower(sunImage2);
        sunflower3 = new Sunflower(sunImage3);
        venus1 = new VenusTrap(venusImage1);
        venus2 = new VenusTrap(venusImage2);
        venus3 = new VenusTrap(venusImage3);
        confuse1 = new ConfusingFlower(confusingFlower1);
        confuse2 = new ConfusingFlower(confusingFlower2);
        flowers.add(sunflower1);
        flowers.add(sunflower2);
        flowers.add(sunflower3);
        flowers.add(venus1);
        flowers.add(venus2);
        flowers.add(venus3);
        flowers.add(confuse1);
        flowers.add(confuse2);
        venusTraps.add(venus1);
        venusTraps.add(venus2);
        venusTraps.add(venus3);
        sunflower1.setX(120);
        sunflower1.setY(91);
        sunflower1.setEnergy(10);
        sunflower2.setX(140);
        sunflower2.setY(281);
        sunflower2.setEnergy(10);
        sunflower3.setX(498);
        sunflower3.setY(547);
        sunflower3.setEnergy(10);
        confuse1.setX(150);
        confuse1.setY(500);
        confuse1.setEnergy(30);
        confuse2.setX(300);
        confuse2.setY(75);
        confuse2.setEnergy(30);
        venus1.setX(441);
        venus1.setY(118);
        venus2.setX(304);
        venus2.setY(361);
        venus3.setX(282);
        venus3.setY(216);
        bumbleBee.setX(542);
        bumbleBee.setY(275);
        honeyBee.setX(0);
        honeyBee.setY(255);
        bumbleBee.setFlowers(flowers);
        anchorPane.setFocusTraversable(true);

    }

    /**
     * onKeyPress handles the right arrow event each time key is pressed
     *
     * @param keyEvent
     */
    @FXML
    public void onKeyPressed(KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.RIGHT) {
            tick();
        }
    }

    /**
     * tick handles movement and tracking of overlapping of bees and flowers
     * tick is activated each time right arrow is pressed
     */
    public void tick() {
        welcomeLabel.setVisible(false);
        welcomeLabel.setDisable(true);
        if (bumbleBee.getEnergy() > 0) {
            bumbleBee.modifyEnergy(); // subtracts 1 energy per tick
            bumbleLife.setText("Bumble Bee Life: " + bumbleBee.getEnergy());
            bumbleTick();
        } else {
            bumbleImage.setVisible(false);
            bumbleLife.setText("Bumble Bee Life: 0");
        }
        if (honeyBee.getEnergy() > 0) {
            honeyBee.modifyEnergy();
            honeyLife.setText("Honey Bee Life: " + honeyBee.getEnergy());
            honeyTick();
        } else {
            honeyImage.setVisible(false);
            honeyLife.setText("Honey Bee Life: 0");
        }
        //Checks if bumble and honey collided
        if (bumbleBee.calculateEuclidean(honeyBee.getX(), honeyBee.getY()) < 45.0) {
            honeyBee.setEnergy(0);
            bumbleBee.setEnergy(0);
            honeyImage.setVisible(false);
            bumbleImage.setVisible(false);
        }

        for (int i = 0; i < flowers.size(); i++) {
            if (honeyBee.calculateEuclidean(flowers.get(i).getX(), flowers.get(i).getY()) < 45.0) {
                honeyBee.addEnergy(flowers.get(i).determineEnergy());
                flowers.get(i).visit();
                if (flowers.get(i) instanceof ConfusingFlower) {
                    if (counter % 10 == 0) {
                        int temp = (int) (Math.random() * 3);
                        if (temp == 1) {
                            honeyBee = new ProtectedBee(honeyBee);
                            honeyImage.setImage(shieldedBee);
                        } else if (temp == 2) {
                            honeyBee = new ObliviousBee(honeyBee);
                            honeyBee.setFlowers(venusTraps);
                            obliviousHoney = true;
                            honeyImage.setImage(confusedBee);
                        } else {
                            honeyBee = new SpeedyBee(honeyBee);
                            honeyImage.setImage(speedy);
                        }
                    }
                    counter++;
                }
            }
        }

        for (int i = 0; i < flowers.size(); i++) {
            if (bumbleBee.calculateEuclidean(flowers.get(i).getX(), flowers.get(i).getY()) < 45.0) {
                bumbleBee.addEnergy(flowers.get(i).determineEnergy());
                flowers.get(i).visit();
                if (flowers.get(i) instanceof ConfusingFlower) {
                    if (counter % 10 == 0) {
                        int temp = (int) (Math.random() * 3);
                        if (temp == 1) {
                            bumbleBee = new ProtectedBee(bumbleBee);
                            bumbleImage.setImage(shieldedBee);
                        } else if (temp == 2) {
                            bumbleBee = new ObliviousBee(bumbleBee);
                            bumbleBee.setFlowers(venusTraps);
                            bumbleImage.setImage(confusedBee);
                        } else {
                            bumbleBee = new SpeedyBee(bumbleBee);
                            bumbleImage.setImage(speedy);
                        }
                    }
                    counter++;
                }
            }
        }
        Alert alert = new Alert(Alert.AlertType.WARNING);
        if (bumbleBee.getEnergy() <= 0 && honeyBee.getEnergy() <= 0) {
            bumbleImage.setVisible(false);
            bumbleBee.setEnergy(0);
            honeyImage.setVisible(false);
            honeyBee.setEnergy(0);

            alert.setTitle("Garden Program");
            alert.setHeaderText("GAME OVER!");
            alert.setContentText("Both Bees have died!");
            alert.showAndWait();

            Platform.exit();
        }

    }

    private void bumbleTick() {
        if (bumbleBee.getEnergy() <= 0) {
            bumbleImage.setVisible(false);
            bumbleBee.setEnergy(0);
        }

        if (bumbleBee.getX() < (flower.getX() + 20) && bumbleBee.getX() > (flower.getX() - 20)) {
            if (bumbleBee.getY() < (flower.getY() + 20) && bumbleBee.getY() > (flower.getY() - 20)) {
                flower = bumbleBee.calculateRandCoordinate();
                xDest = Math.abs(bumbleImage.getLayoutX() - flower.getX()) / bumbleBee.getSpeed();
                yDest = Math.abs(bumbleImage.getLayoutY() - flower.getY()) / bumbleBee.getSpeed();
            }
        } else if (!firstTick) {
            flower = bumbleBee.calculateRandCoordinate();
            xDest = Math.abs(bumbleImage.getLayoutX() - flower.getX()) / bumbleBee.getSpeed();
            yDest = Math.abs(bumbleImage.getLayoutY() - flower.getY()) / bumbleBee.getSpeed();
            firstTick = true;
        }
        if (bumbleImage.getLayoutX() <= flower.getX()) {
            bumbleBee.setX(bumbleBee.getX() + xDest);
            bumbleImage.setLayoutX(bumbleBee.getX());
        } else {
            bumbleBee.setX(bumbleBee.getX() - xDest);
            bumbleImage.setLayoutX(bumbleBee.getX());
        }
        if (bumbleImage.getLayoutY() < flower.getY()) {
            bumbleBee.setY(bumbleBee.getY() + yDest);
            bumbleImage.setLayoutY(bumbleBee.getY());
        } else {
            bumbleBee.setY(bumbleBee.getY() - yDest);
            bumbleImage.setLayoutY(bumbleBee.getY());
        }
    }

    private void honeyTick() {
        if (!obliviousHoney) {
            if (honeyImage.getLayoutY() < 565) {
                honeyBee.setY((honeyBee.getY() + 10) * honeyBee.getSpeedMultiplier());
                honeyImage.setLayoutY((honeyImage.getLayoutY() + 10) * honeyBee.getSpeedMultiplier());
            } else {
                honeyImage.setX(honeyImage.getX() + 100);
                honeyBee.setX(honeyBee.getX() + 100);
                honeyImage.setLayoutY(0);
                honeyBee.setY(0);
            }

            if (honeyImage.getX() > 565) {
                honeyBee.setX(0);
                honeyImage.setLayoutX(0);
                honeyBee.setY(0);
                honeyImage.setLayoutY(0);
            }

        } else {
            if (honeyBee.getX() < (tempFlower.getX() + 20) && honeyBee.getX() > (tempFlower.getX() - 20)) {
                if (honeyBee.getY() < (tempFlower.getY() + 20) && honeyBee.getY() > (tempFlower.getY() - 20)) {
                    tempFlower = honeyBee.calculateRandCoordinate();
                    xDestination = Math.abs(honeyImage.getLayoutX() - tempFlower.getX()) / honeyBee.getSpeed();
                    yDestination = Math.abs(honeyImage.getLayoutY() - tempFlower.getY()) / honeyBee.getSpeed();
                }
            } else if (!firstObliviousTick) {
                tempFlower = honeyBee.calculateRandCoordinate();
                xDestination = Math.abs(honeyImage.getLayoutX() - tempFlower.getX()) / honeyBee.getSpeed();
                yDestination = Math.abs(honeyImage.getLayoutY() - tempFlower.getY()) / honeyBee.getSpeed();
                firstObliviousTick = true;
            }
            if (honeyImage.getLayoutX() <= tempFlower.getX()) {
                honeyBee.setX(honeyBee.getX() + xDestination);
                honeyImage.setLayoutX(honeyBee.getX());
            } else {
                honeyBee.setX(honeyBee.getX() - xDestination);
                honeyImage.setLayoutX(honeyBee.getX());
            }
            if (honeyImage.getLayoutY() < tempFlower.getY()) {
                honeyBee.setY(honeyBee.getY() + yDestination);
                honeyImage.setLayoutY(honeyBee.getY());
            } else {
                honeyBee.setY(honeyBee.getY() - yDestination);
                honeyImage.setLayoutY(honeyBee.getY());
            }
        }
    }
}
