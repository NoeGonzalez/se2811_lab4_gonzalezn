/**
 * SE2811
 * Winter 2017
 * Lab 3 The Flowers and the Bees - HoneyBee Class
 * Name: Noe Gonzalez & Trevor Suarez
 * Created: 1/9/2018
 */

package se2811;

import javafx.scene.image.ImageView;

import java.util.ArrayList;

/**
 * @author Trevor Suarez
 * HoneyBee provides functionality for position and energy values for the HoneyBee, the non-random bee
 */
public class HoneyBee extends Bee {

    private int energy;
    private double x;
    private double y;
    private int speed;
    private ImageView image;
    private ArrayList<Flower> flowers;

    /**
     * HoneyBee constructor initializes HoneyBee and its values
     *
     * @param image
     */
    public HoneyBee(ImageView image) {
        energy = 1000;
        this.x = image.getLayoutX();
        this.y = image.getLayoutY();
        this.image = image;
        this.speed = 50;

    }

    public void setFlowers(ArrayList<Flower> flowers) {
        this.flowers = flowers;
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int newEnergy) {
        energy = newEnergy;
    }

    /**
     * modifyEnergy subtracts one energy value when one tick is completed
     */
    public void modifyEnergy() {
        energy = energy - 1;
    }

    /**
     * addEnergy adds energy when hovered over flower
     *
     * @param addEnergy amount of energy being added to bee
     */
    public void addEnergy(int addEnergy) {
        energy = energy + addEnergy;
    }

    public double getSpeed() {
        return speed;
    }
}