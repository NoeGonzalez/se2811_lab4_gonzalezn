package se2811;

public class ProtectedBee extends BeeDecorator {
    private Bee bee;
    private int shieldHealth;

    public ProtectedBee(Bee bee) {
        super(bee);
        this.bee = bee;
        this.shieldHealth = 400;
    }

    @Override
    public int getEnergy() {
        return bee.getEnergy();
    }

    @Override
    public void setEnergy(int newEnergy) {
        bee.setEnergy(newEnergy);
    }

    /**
     * modifyEnergy subtracts one energy value when one tick is completed
     */
    @Override
    public void modifyEnergy() {
        bee.modifyEnergy();
    }

    /**
     * addEnergy adds energy when hovered over flower
     *
     * @param addEnergy amount of energy being added to bee
     */
    @Override
    public void addEnergy(int addEnergy) {
        int temp = shieldHealth;
        shieldHealth = 0;
        bee.setEnergy(addEnergy + temp);
    }

    @Override
    public double getSpeed() {
        return bee.getSpeed();
    }

    @Override
    public int getSpeedMultiplier() {
        return bee.getSpeedMultiplier();
    }
}
